#!/usr/bin/env python3
"""
Starnix

A *nix python program template

(C) Dale Magee, 2022
Public Domain

This file is a template - you should erase this entire comment block and write 
 something meaningful here.

This template includes a bunch of the basics that you'll need to write a
 unix-compatible CLI program in python.
 
Things this template includes:

* execute permissions (chmod a+x starnix.py)

* A shebang 
	- see line 1
	- must be line 1
	- line must start with '#!'
	- must point to the interpreter for the language your program is written in
		(e.g /bin/bash for oldschool bash and 30 years of backwards compatibility)
	- /usr/bin/env is a tool on modern unixes which gets you into a standard environment
	- python3 is an argument to /usr/bin/env. This effectively means "run the python3 that's in the path"
		
* stdin/stdout/stderr, and how these are tied to file I/O
		
* exit codes
		
* command line arguments


Other unix things not (yet?) covered here:

* standard useful patterns (e.g handling either stdin or an --inputfile argument )

* logging, log levels, verbosity, and using the python logging library to make it all easy

* standard config file locations (/etc, ~/.config/<program>, and ~/.<program>)

* temp files, why they're often insecure, how to do them securely, and how python makes this super easy

"""

# Some consts we might use in a few places
# It would be wise for you to e.g put these in a module, and 
#  load your version number from a file
PROGRAM_NAME = "A *nix program"
PROGRAM_VERSION = 0.01
PROGRAM_DESCRIPTION = "Does things in a unix-compatible way."

WELCOME_MSG = (f"{PROGRAM_NAME}, v{PROGRAM_VERSION}.\n\n" + 
"""
This template is a pretty useless demonstration program that does simple string substitution
 and demonstrates how to write a unix-compatible CLI program in python

You should print things like informational and status messages to stderr, not stdout.
stdout should be your program's output and may e.g be piped to a file or another program,
stderr is intended to be seen by the user or piped to e.g a log file.

If you've invoked this program with no arguments, it will read and write to the
 terminal's input/output interactively, processsing each line you enter.

In interactive mode you can send an EOF character and terminate the program 
 by pressing CTRL-D.

"""
)

import sys			# sys is used for a bunch of unix sstuff. Notably, stdin/stdout/stderr
import argparse		# argparse is part of the stdlib, gives us an easy API for handling command-line options

def parse_cmdline():
	"""
	Sets up an arg parser, parse the command line, and returns a populated list of options.
	
	The most notable things argparse gives you are:
	- easily set up/handle/validate command-line arguments without worrying about e.g order
	- automatic --help / -h argument which shows usage message with argument descriptions
	
	But it has many, many more options, see: 
	 https://docs.python.org/3/library/argparse.html
	
	"""
	parser = argparse.ArgumentParser(description=PROGRAM_DESCRIPTION)
	
	"""
	parser.add_argument(
		"-o","--option", 	# short version, long version
		type=int, 
		dest="option", 		# the name of the var in the returned options
		metavar="NUM", 		# the name of the var in help "--option NUM". equal to dest if not provided.
		default=42,
		help="An integer option"
	)
	"""
	
	parser.add_argument(
		"-s","--string",
		type=str,
		metavar="string",
		dest="stringval",
		help="Value to replace 'foo' with",
	)
	
	parser.add_argument(		# A Boolean argument
		"-q", "--quiet",
		action="store_const",	# action to take if the arg is provided
		const=True,				# value to store in dest (see action)
		dest="quiet",			
		default=False,
		help="Show less output"
	)
	
	"""
	parser.add_argument(
		"-r", "--required",
		required=True,			# required.
		type=str,
		metavar="thing",
		dest="requiredval",
		help="A required option"
	)
	"""
	
	# add more arguments here
	
	return parser.parse_args()
	
def errprint(msg:str):
	"""
	Similar to print(), but prints to stderr rather than stdout.
	"""
	sys.stderr.write(f"{msg}\n")

def do_stuff(opts):
	"""
	This is your 'Main' method, where you do the thing that your program does
	
	In our example, we're going to read a line from stdin, do something to 
	 the data, and write it to stdout.
	
	Your program doesn't *have* to read from stdin and/or write to stdout, 
	 but if it makes sense to do so (and it usually does), you should - 
	 doing so gives you access to the power of pipes, and using all the 
	 standard unix utilities with your program, e.g:

	 echo "something" | ./your_program.py | awk '{print "something "$1}'
	
	stdin/out/err are accessible as file-like object via the sys library, as
	 sys.stdin, sys.stdout, and sys.stderr
	
	"""
	
	while True:
		input=sys.stdin.readline()	# blocks until we have a line of input*. We could also use read(), which blocks until EOF
									# You'll usually deal with text, but this may also be binary data, e.g
									#   cat /some/jpeg.jpg | ./your_program.py
									# the line that you read will usually end with a linefeed, but at EOF you may get a line
									#  without one
		
		if input == "":				# no input, i.e EOF
			break
			
		if input == "\n":			# empty line (if interactive, this means the user just pressed enter)
			print("empty line!")	# print is equivalent to sys.stdout.write(val + "\n")
			continue
		
		# we want to use write here, not print, because we don't want to append a linefeed
		sys.stdout.write(
			input.replace('foo',opts.stringval)
		)
	
if __name__ == "__main__":
	"""
	Entry Point
	"""
	
	opts = parse_cmdline()
	
	if not opts.quiet:
		errprint(WELCOME_MSG)
	
	if opts.stringval is None:	# this could also be done with a default value
		opts.stringval = "bar"
	
	if opts.stringval == "foo":
		errprint("ERROR: can't replace 'foo' with 'foo'!") # this could also be done with required=True on the argument.
		
		"""
		Exit the program with a non-zero exit code.
		
		exit code is a byte, i.e an int between 0-255.
		
		unix exit codes do things backwards from most programs 
		with regards to "truthiness", i.e an exit code of 
		0 signals success/true in unix, a non-zero exit code signals failure/false.
		
		exit codes are program-specific: you can simply invent your own,
		but there are a bunch of informal conventions, see:
		 	https://tldp.org/LDP/abs/html/exitcodes.html
		
		You may use any of these that you feel apply, or none, 
			but if your exit codes have special meaning, 
			you should document them
			
		Note that 0 is the default exit code, you don't need a sys.exit(0)
			at the end of your program.
			
		"""
		sys.exit(1)
		
		
	do_stuff(opts)
	
	